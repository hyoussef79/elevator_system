require "rails_helper"

RSpec.describe BuildingsController, type: :routing do
  describe "routing" do
    it "routes to #show" do
      expect(:get => "/buildings/1").to route_to("buildings#show", :id => "1")
    end
  end
end
