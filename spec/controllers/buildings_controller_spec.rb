require 'rails_helper'

RSpec.describe BuildingsController, type: :controller do

  describe "GET #show" do
    it "returns a success response" do
      building = FactoryBot.create(:building)
      get :show, params: {id: building.to_param}
      expect(response).to be_success
    end
  end
end
