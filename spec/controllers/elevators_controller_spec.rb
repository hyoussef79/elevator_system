require 'rails_helper'

RSpec.describe ElevatorsController, type: :controller do

  describe "GET #show" do
    it "returns http success" do
      building = FactoryBot.create(:building)
      elevator = FactoryBot.create(:elevator, building: building)
      get :show, params: {building_id: building.id, id: elevator.id}
      expect(response).to have_http_status(:success)
    end
  end

end
