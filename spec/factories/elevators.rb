FactoryBot.define do
  factory :elevator do
    current_floor 1
    direction "MyString"
    building nil
  end
end
