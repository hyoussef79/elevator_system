class BuildingsController < ApplicationController
  before_action :set_building, only: [:show, :request_elevator]

  # GET /buildings/1
  def show
    render json: @building
  end

  def request_elevator
    return head :unprocessable_entity unless valid_request?
    return head :unprocessable_entity if requests_limit_reached?
    @order = @building.elevator_orders.new(request_params)
    @order.kind = 'request_elevator'
    @order.state = 'pending'
    @order.elevator = Elevator.first

    @order.save

    render json: @order
  end

  private

  def set_building
    @building = Building.find(params[:id])
  end

  def request_params
    params.permit(:floor, :direction)
  end

  def valid_request?
    floor, direction = params[:floor], params[:direction]
    valid_directions = ['up', 'down']
    return false unless floor.is_a?Integer
    return false unless valid_directions.include?(direction)
    (0...@building.floors_count).include? floor
  end

  def requests_limit_reached?
    # Should be updated to support more requests
    ElevatorOrder.count == 1
  end
end
