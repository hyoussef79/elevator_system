class ElevatorsController < ApplicationController
  before_action :set_elevator, only: [:show, :select_floor, :run_step]

  def show
    render json: @elevator
  end

  def select_floor
    return head :unprocessable_entity unless valid_request?
    if @order.state == 'ready'
      @order.update(
        kind: 'select_floor',
        floor: @floor,
        direction: '',
        state: 'on_progress'
      )
      render(
        json: { message: 'Order received' },
        status: :ok
      )
    else
      render(
        json: { message: 'Elevator is not ready yet to receive your order' },
        status: :unprocessable_entity
      )
    end
  end

  def run_step
    return head :ok unless @elevator.elevator_orders.any?
    @order = @elevator.elevator_orders.first
    @target_floor = @order.floor
    @current_floor = @elevator.current_floor
    if @target_floor > @current_floor
      @next_floor = @current_floor + 1
      @elevator.update(current_floor: @next_floor, direction: 'up')
      message = build_message
    elsif @target_floor < @current_floor
      @next_floor = @current_floor - 1
      @elevator.update(current_floor: @next_floor, direction: 'down')
      message = build_message
    else
      if @order.kind == 'select_floor'
        message = 'Elevator has arrived to the requested floor'
        @order.destroy
      else
        @order.update(state: 'ready')
        message = 'Elevator is ready, please select the required floor'
      end
      @elevator.update(direction: '')
    end
    render(
      json: { message: message },
      status: :ok
    )
  end

  def run_until_stop
  end

  private

  def set_elevator
    @elevator = Elevator.find(params[:id])
  end

  def valid_request?
    @order = ElevatorOrder.find_by(id: params[:order_id])
    return false unless @order.present?
    @floor = params[:floor]
    return false unless @floor.is_a? Integer
    (0...@elevator.floors_count).include? @floor
  end

  def build_message
    "#{@current_floor}..=>..#{@next_floor}"
  end
end
