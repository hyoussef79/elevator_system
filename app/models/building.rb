class Building < ApplicationRecord
  has_many :elevators
  has_many :elevator_orders
end
