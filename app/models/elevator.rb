class Elevator < ApplicationRecord
  belongs_to :building
  has_many :elevator_orders

  delegate :floors_count, to: :building
end
