class ElevatorOrder < ApplicationRecord
  belongs_to :building
  belongs_to :elevator
end
