class ElevatorOrderSerializer < ActiveModel::Serializer
  attributes :id, :kind, :state, :direction, :floor, :building_id, :elevator_id
end
