class BuildingSerializer < ActiveModel::Serializer
  attributes :id, :floors_count
end
