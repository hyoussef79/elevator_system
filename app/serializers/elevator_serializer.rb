class ElevatorSerializer < ActiveModel::Serializer
  attributes :id, :current_floor, :direction
end
