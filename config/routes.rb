Rails.application.routes.draw do

  resources :buildings, only: [:show] do
    post 'request_elevator', on: :member
    resources :elevators, only: [:show] do
      member do
        post 'select_floor'
        get 'run_step'
        get 'run_until_stop'
      end
    end
  end
end
