# Elevator System
This API app is a simple simulator for an elevator system.
(Ruby version: 2.4.3, Rails version: 5.1.0).

## Initial Assumptions
The following initial assumptions are taken into consideration:  
1- The App has a 6 floors building and an elevator which belongs to it, both are created through the seed file.  
2- It's not possible to add another elevator or to update the floors_count.  
3- One request is supported at a time for now, new requests will be ignored until the active request is processed.

# Request an elevator
Send a POST request to /buildings/1/request_elevator with the ("from_floor" & "desired_direction") included in the request body.
```
POST /buildings/1/request_elevator
body { "floor": 4, "direction": "down" }

response { "id": 2, "elevator_id": 1, ...etc}
```

# Select a floor number
The elevator is ready to receive a "select_floor" order, ONLY when it arrives to the correct floor where it was requested.  
Using the "order_id" from the request_elevator's response, you can send a select_floor order as follows:
```
POST /buildings/1/elevators/1/select_floor
body { "order_id": 2, floor: 0 }

ex1 response { message: 'Order received' }
ex2 response { message: 'Elevator is not ready yet to receive your order' }
```

# Run the elevator
For now the elevator doesn't move automatically, user need to send a run_step request to move the elevator 1 step.  
You can then see from the response either a move from a floor to another, or a message showing the situation.
```
GET /buildings/1/elevators/1/run_step

ex1 response { "message": "0..=>..1" }
ex2 response { "message": "Elevator is ready, please select the required floor" }
ex3 response { "message": "Elevator has arrived to the requested floor" }
```

# Deployment
App is deployed to https://elevator-system.herokuapp.com  

# Other available end points
```
GET  /buildings/1
GET  /buildings/1/elevators/1
```

## To Do
* Cleanup/Refactoring
* Writing tests
* Implement run_until_stop ability
* Dealing with concurrent requests
* Better Error handling / covering more corner-cases
* Better Documentation
