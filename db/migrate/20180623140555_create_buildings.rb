class CreateBuildings < ActiveRecord::Migration[5.1]
  def change
    create_table :buildings do |t|
      t.integer :floors_count, null: false, default: 0

      t.timestamps
    end
  end
end
