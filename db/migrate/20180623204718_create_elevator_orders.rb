class CreateElevatorOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :elevator_orders do |t|
      t.string :kind, null: false
      t.string :state, null: false
      t.string :direction
      t.integer :floor
      t.references :building, foreign_key: true
      t.references :elevator, foreign_key: true

      t.timestamps
    end
  end
end
