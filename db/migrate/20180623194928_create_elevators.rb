class CreateElevators < ActiveRecord::Migration[5.1]
  def change
    create_table :elevators do |t|
      t.integer :current_floor, null: false, default: 0
      t.string :direction
      t.references :building, foreign_key: true

      t.timestamps
    end
  end
end
