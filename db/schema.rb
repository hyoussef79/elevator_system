# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180623204718) do

  create_table "buildings", force: :cascade do |t|
    t.integer "floors_count", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "elevator_orders", force: :cascade do |t|
    t.string "kind", null: false
    t.string "state", null: false
    t.string "direction"
    t.integer "floor"
    t.integer "building_id"
    t.integer "elevator_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["building_id"], name: "index_elevator_orders_on_building_id"
    t.index ["elevator_id"], name: "index_elevator_orders_on_elevator_id"
  end

  create_table "elevators", force: :cascade do |t|
    t.integer "current_floor", default: 0, null: false
    t.string "direction"
    t.integer "building_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["building_id"], name: "index_elevators_on_building_id"
  end

end
